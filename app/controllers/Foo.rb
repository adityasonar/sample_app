class Foo < ApplicationController
    def bar
        render html: "bar method"
    end

    def baz
        render html: "baz method"
    end
end
